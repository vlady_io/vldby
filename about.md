---
layout: page
title: Контакты
permalink: /contacts/
---
Блог располагается на <a href="https://netlify.com">Netlify</a>, я использую <a href="https://jekyllrb.com/">Jekyll</a> для его генерации, а стили написаны мною - <a href="https://gitlab.com/vlady_io/vldby/blob/master/_sass/_base.scss">название им</a> я ещё не придумал. Благодарю <a href="http://tonsky.me/">Никиту Прокопова</a> за его личный сайт, с него я и вдохновлялся.

Со мной можно связаться этими способами:
<ul>
    <li><a href="mailto:me@vld.by" title="Написать на мою почту me@vld.by">me@vld.by</a></li>
    <li><a href="https://www.linkedin.com/in/vlad-andreev/">Linkedin</a></li>
    <li><a href="https://github.com/vladyio">GitHub</a></li>
    <li><a href="https://telegram.me/and_vlad">Telegram</a></li>
    <li><a href="https://twitter.com/vlady_io">Twitter</a></li>
</ul>
